/* vi:set et ai sw=4 sts=4 ts=4: */
/*-
 * Copyright (c) 2024 Stephen Robinson <stephen@drsudo.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifdef G_ENABLE_DEBUG
#define DEBUG_G_MESSAGE(...) g_message(__VA_ARGS__)
#else
#define DEBUG_G_MESSAGE(...) do {} while(0)
#endif

#endif
