/* vi:set et ai sw=4 sts=4 ts=4: */
/*-
 * Copyright (c) 2017 Frederik Möllers <frederik@die-sinlosen.de>
 * Copyright (c) 2024 Stephen Robinson <stephen@drsudo.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "debug.h"
#include "nextcloud-socket.h"

#define NC_CLIENT_SOCKET_RELPATH "/Nextcloud/socket"

static struct timeval socket_timeout = {.tv_sec = 0, .tv_usec = 500000};

static bool is_shutdown = false;

static int socket_fd = -1;
static GIOChannel * socket_channel = NULL;
static guint socket_event_source_id = 0;
static guint socket_event_source_id_2 = 0;

static char * version = NULL;
static GList * synced_dirs = NULL;
static GHashTable * strings = NULL;

static char * split_on_char(char *, char);
static int handle_response(char * cmd, char * args);

static void disconnect_socket(void)
{
    if (socket_channel != NULL) {
        g_source_remove(socket_event_source_id);
        g_io_channel_unref(socket_channel);
    }
    if (socket_fd >= 0) {
        close(socket_fd);
    }

    if (strings != NULL) {
        g_hash_table_destroy(strings);
        strings = NULL;
    }

    if (version != NULL) {
        g_free(version);
        version = NULL;
    }
}

static gboolean handle_io_watch_err(GIOChannel * source,
                                    GIOCondition condition,
                                    gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }
    g_warning("Recieved error GIOCondition: %d. Reconnecting...", condition);
    ncsock_initialize();
    return FALSE;
}

static gboolean handle_io_watch(GIOChannel * source,
                                GIOCondition condition,
                                gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }
    char * line = NULL;
    gsize term_pos = 0;
    while (g_io_channel_read_line(source, &line, NULL, &term_pos, NULL) == G_IO_STATUS_NORMAL) {
        line[term_pos] = '\0'; // Remove newline char
        char * args = split_on_char(line, ':');
        if (args == NULL) {
            g_warning("Received malformed Nextcloud message line: %s", line);
        }
        if (handle_response(line, args) != 0) {
            DEBUG_G_MESSAGE("Received command: %s:%s", line, args);
        }
        g_free(line);
    }
    return TRUE;
}

static gboolean connect_socket(gpointer data)
{
    if (is_shutdown) {
        return FALSE;
    }

    /* Create local socket. */
    socket_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        g_warning("socket() failed! %s", strerror(errno));
        return TRUE;
    }

    if(setsockopt(socket_fd,
                  SOL_SOCKET,
                  SO_RCVTIMEO,
                  (char*) &socket_timeout,
                  sizeof(socket_timeout)) < 0 ||
       setsockopt(socket_fd,
                  SOL_SOCKET,
                  SO_SNDTIMEO,
                  (char*) &socket_timeout,
                  sizeof(socket_timeout)) < 0)
    {
        close(socket_fd);
        g_warning("setsockopt() failed! %s", strerror(errno));
        return TRUE;
    }

    /*
    * For portability clear the whole structure, since some
    * implementations have additional (nonstandard) fields in
    * the structure.
    */
    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(struct sockaddr_un));

    /* Connect socket to socket address */
    addr.sun_family = AF_UNIX;

    char const* xdg_runtime_dir = g_getenv("XDG_RUNTIME_DIR");
    if (xdg_runtime_dir == NULL || *xdg_runtime_dir == '\0') {
        close(socket_fd);
        g_warning("XDG_RUNTIME_DIR is unset");
        return TRUE;
    }

    snprintf(addr.sun_path, sizeof(addr.sun_path),
             "%s%s", xdg_runtime_dir, NC_CLIENT_SOCKET_RELPATH);

    if (connect(socket_fd, (const struct sockaddr*) &addr, sizeof(struct sockaddr_un)) < 0)
    {
        g_warning("connect() failed! %s", strerror(errno));
        close(socket_fd);
        return TRUE;
    }

    DEBUG_G_MESSAGE("Connected to '%s'", addr.sun_path);

    socket_channel = g_io_channel_unix_new(socket_fd);
    socket_event_source_id = g_io_add_watch(socket_channel,
                                            G_IO_IN,
                                            handle_io_watch,
                                            NULL);

    socket_event_source_id_2 = g_io_add_watch(socket_channel,
                                              G_IO_HUP | G_IO_ERR | G_IO_NVAL,
                                              handle_io_watch_err,
                                              NULL);

    ncsock_send(NC_CMD_VERSION);
    ncsock_send(NC_CMD_GET_STRINGS);

    return FALSE;
}

void ncsock_initialize(void)
{

    // This is here in case we are re-initializing
    disconnect_socket();

    strings = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);

    if (connect_socket(NULL)) { // NOTE: returns TRUE on error!
        g_timeout_add(5000, connect_socket, NULL);
    }
}

void ncsock_shutdown(void) {
    is_shutdown = true;
    disconnect_socket();
}

bool ncsock_connected(void)
{
    return socket_fd >= 0;
}

int ncsock_send(const char * cmdfmt, ...)
{

    if (socket_channel == NULL) {
        g_warning("Trying to send Nextcloud command to closed socket");
        return -1;
    }

    va_list cmdargs;
    va_start(cmdargs, cmdfmt);

    char* cmd = g_strdup_vprintf(cmdfmt, cmdargs);
    ssize_t len = strlen(cmd);
    ssize_t sent = send(socket_fd, cmd, strlen(cmd), 0);
    if (sent < len) {
        g_warning("Unable to send command! %s\n", strerror(errno));
        ncsock_initialize(); // Reconnect
        return -1;
    }

    g_free(cmd);

    return 0;
}

bool ncsock_is_in_registered_paths(const char * dirpath)
{
    char absdir[PATH_MAX];
    if (realpath(dirpath, absdir) == NULL) {
        return false;
    }

    for (GList* sync_dir = synced_dirs; sync_dir != NULL; sync_dir = sync_dir->next) {
        char abssync[PATH_MAX + 1];
        if (realpath(sync_dir->data, abssync) == NULL) {
            continue;
        }
        if (strncmp(abssync, absdir, strlen(abssync)) == 0) {
            return true;
        }
    }
    return false;
}

static char * split_on_char(char * s, char c)
{
    char * delim = strchr(s, c);
    if (delim == NULL) {
        return NULL;
    }
    *delim = 0;
    return delim + 1;
}

static int handle_cmd_string(char * args)
{
    char * value = split_on_char(args, ':');
    if (value == NULL) {
        g_warning("Recieved STRING args with no colon: %s", args);
        return -1;
    }

    DEBUG_G_MESSAGE("Recieved STRING (key: %s, value: %s)", args, value);
    g_hash_table_insert(strings, g_strdup(args), g_strdup(value));

    return 0;
}

static int handle_cmd_version(char * args)
{
    DEBUG_G_MESSAGE("Recieved VERSION: %s", args);
    version = g_strdup(args);
    return 0;
}

static int handle_register_path(char * args)
{
    DEBUG_G_MESSAGE("Registering path: %s", args);
    synced_dirs = g_list_append(synced_dirs, g_strdup(args));
    return 0;
}

static int handle_unregister_path(char * args)
{
    synced_dirs = g_list_append(synced_dirs, args);

    GList* item = g_list_find_custom(synced_dirs, args, (GCompareFunc)strcmp);
    if (item) {
        synced_dirs = g_list_remove_link(synced_dirs, item);
        g_list_free_full(item, g_free);
        DEBUG_G_MESSAGE("Unregistered directory: %s", args);
    } else {
        g_warning("Failed to unregister directory: %s", args);
    }

    if (synced_dirs == NULL) {
        // Last synced dir was removed. We should try to reconnect.
        ncsock_initialize();
    }

    return 0;
}

static int handle_response(char * cmd, char * args)
{
    if (strcmp(cmd, "STRING") == 0) {
        return handle_cmd_string(args);
    } else if (strcmp(cmd, "VERSION") == 0) {
        return handle_cmd_version(args);
    } else if (strcmp(cmd, "REGISTER_PATH") == 0) {
        return handle_register_path(args);
    } else if (strcmp(cmd, "UNREGISTER_PATH") == 0) {
        return handle_unregister_path(args);
    } else {
        return -1;
    }
}
